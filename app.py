from flask import Flask
import logging as logger

logger.basicConfig(level="DEBUG")

flaskAppInstance = Flask(__name__)

if __name__ == '__main__':
    logger.debug("Starting Flask Server")
    from api import *

    flaskAppInstance.run(host="0.0.0.0", port=5002, debug=True, use_reloader=True)


@flaskAppInstance.route('/', methods=['GET'])
def index():
    return 'Security App!'

