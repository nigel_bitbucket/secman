#!/bin/bash

#DEBUG
echo "DEP_var : $deploymentvariable"
echo "BEFORE"
cat deploymentenv/secman/${AWS_EKS_ENV}-deploymentenv.sh
#change
case "$deploymentvariable" in
	green*)
	echo "in green"
	  printf "deploymentvariable=blue\nstage_deploymentvariable=green">deploymentenv/secman/${AWS_EKS_ENV}-deploymentenv.sh
	  ;;
	blue*)
	echo "in blue"
	  printf "deploymentvariable=green\nstage_deploymentvariable=blue">deploymentenv/secman/${AWS_EKS_ENV}-deploymentenv.sh
	  ;;
	*)
	echo "variable [$deploymentvariable] unrecognized"
esac

#DEBUG
echo "AFTER"
cat deploymentenv/secman/${AWS_EKS_ENV}-deploymentenv.sh