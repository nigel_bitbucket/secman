from flask_restful import Resource
from flask import request
import logging as logger
import db
import json
from uuid import uuid4


class Task(Resource):

    def get(self):
        db.cur.execute("select id, username, name, status from user;")

        result = db.cur.fetchall()
        #       db.conn.close()
        # return the results!
        logger.debug("Inside get method of Task")
        return json.dumps(result), 200

    def post(self):
        id = request.json['id']
        username = request.json['username']
        password = request.json['password']
        name = request.json['name']
        status = request.json['status']

        sql = "insert into user (id,username,password,name,status) values (%s,%s,%s,%s,%s)"
        db.cur.execute(sql, (id, username, password, name, status))

        db.conn.commit()
        logger.debug("Inisde the post method of Task")
        return {"message": "Added New User - {}".format(name)}, 200


# get token
class Task1(Resource):

    def post(self):
        username = request.headers['username']
        password = request.headers['password']

        sql = "select count(id) from user where username = %s and password = %s and status = 1"
        db.cur.execute(sql, (username, password))
        result = db.cur.fetchone()
        result = (result['count(id)'])

        if result > 0:
#            rand_token = 'Correct!'
            rand_token = str(uuid4())
            sql = "update user set token = %s where username = %s and password = %s"
            db.cur.execute(sql, (rand_token, username, password))
        else:
            rand_token = 'Incorrect Username and Password'

        db.conn.commit()
        logger.debug("Inside get method of Task")
        return (result, username, password, rand_token), 200

# validate token
class Task2(Resource):

    def get(self):
        token = request.headers['token']

        sql = "select count(id) from user where token = %s and status = '1'"
        db.cur.execute(sql, token)
        result = db.cur.fetchone()
        result = (result['count(id)'])

        if result > 0:
            rand_token = 'Token Validated!'
        else:
            rand_token = 'Invalid Token!'
        logger.debug("Inside get method of Task result - {}".format(result))
#        return (result, rand_token), 200
        return result, 200
