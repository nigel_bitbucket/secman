from flask_restful import Api
from app import flaskAppInstance
from .TaskAPI import Task
from .TaskAPI import Task1
from .TaskAPI import Task2
from .TaskByID2API import TaskByID
from .TaskByID2API import TaskByID1

restServerInstance = Api(flaskAppInstance)

restServerInstance.add_resource(Task, "/api/v1.0/user/")
restServerInstance.add_resource(Task1, "/api/v1.0/user/token")
restServerInstance.add_resource(Task2, "/api/v1.0/user/validate")
restServerInstance.add_resource(TaskByID, "/api/v1.0/user/<string:uid>")
restServerInstance.add_resource(TaskByID1, "/api/v1.0/user/remove/<string:uid>")

