from flask_restful import Resource
import logging as logger
import db
import json
from flask import request


# Get User by ID
class TaskByID(Resource):

    def get(self, uid):
        sql = "select id, username, name, status from user where id = %s"
        db.cur.execute(sql, uid, )

        result = db.cur.fetchall()

        # return the results!
        logger.debug("Inisde the get method of TaskById. TaskID = {}".format(uid))
        return json.dumps(result), 200


# Remove User
class TaskByID1(Resource):

    def get(self, uid):
        sql = "select id, username, name, status from user where id = %s"
        db.cur.execute(sql, uid, )

        result = db.cur.fetchall()

        # return the results!
        logger.debug("Inisde the get method of TaskById. TaskID = {}".format(uid))
        return json.dumps(result), 200

    def put(self, uid):
        sql = "update user set status = 0 where id = %s"
        db.cur.execute(sql, uid, )
        db.conn.commit()
        logger.debug("Inisde the put method of TaskByID. TaskID = {}".format(uid))
        return {"message": "Removed User, ID = {}".format(uid)}, 200
