#!/bin/bash

#download
wget https://eks-cluster-variable.s3.us-east-2.amazonaws.com/satman/${AWS_EKS_ENV}-deploymentenv.sh -O satman-deploymentenv.sh
wget https://eks-cluster-variable.s3.us-east-2.amazonaws.com/locman/${AWS_EKS_ENV}-deploymentenv.sh -O locman-deploymentenv.sh
wget https://eks-cluster-variable.s3.us-east-2.amazonaws.com/secman/${AWS_EKS_ENV}-deploymentenv.sh -O secman-deploymentenv.sh

#read and set vars
source satman-deploymentenv.sh
echo "satmandeploymentvariable=$stage_deploymentvariable" > microserviceenvs.sh
source secman-deploymentenv.sh
echo "secmandeploymentvariable=$deploymentvariable" >> microserviceenvs.sh
source locman-deploymentenv.sh
echo "locmandeploymentvariable=$stage_deploymentvariable" >> microserviceenvs.sh

chmod a+x microserviceenvs.sh